// Constantes
const urlNotes = "http://localhost:3000/Notes/"
const urlProprietaires = "http://localhost:3000/Proprietaires/"

// Objets JS :
function Note(id, titre, proprietaireId, contenu, important){
    this.id = id;
    this.titre = titre;
    this.proprietaireId = proprietaireId;
    this.contenu = contenu;
    this.important = important
}

function Proprietaire(id, nom, prenom){
    this.id = id;
    this.nom = nom;
    this.prenom = prenom;
}

// Fonctions API note
function recupererNotes(){ //récupère toutes les notes de l'API
    listeNotes = new Array();
    console.log("recupererNotes")
    $.ajax({
        url: urlNotes,
        type: "GET",
        async: false,
        dataType : "json",
        success: function(notes){
            notes.forEach(function(elem) {
                listeNotes.push(
                    new Note(elem.id, elem.titre, elem.proprietaireId, elem.contenu, elem.important)
                );
            });
            remplirNavNotes(listeNotes);
        },
        error: function(req, status, err) {
            window.alert("Impossible de récupérer les notes !");
        }
    });
    return listeNotes;
}

function recupererNote(id){ //récupère une note de l'API
    $.ajax({
        url: urlNotes+id,
        type: "GET",
        async: false,
        dataType : "json",
        success: function(noteJSON){
            let n = new Note(noteJSON.id, noteJSON.titre, noteJSON.proprietaireId, noteJSON.contenu, noteJSON.important);
            return n;
        },
        error: function(req, status, err) {
            window.alert("Impossible de récupérer la note "+id+" !");
        }
    });
}

function sauvegarderNouvelleNote(){ // sauvegarde la nouvelle note dans l'API
    let note = new Note(
        getIdMaxNote(),
        $("#titre").val(),
        Number( $("#proprio").val() ),
        $("#contenu").val(),
        ($("#imp").attr('src') == "img/etoile_rempli.png")
    );
    console.log(JSON.stringify(note));
    $.ajax({
        url: urlNotes,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(note),
        dataType: 'json',
        success: function(){
            alert('Sauvegarde de la nouvelle note réussie !');
            refresh();
        },
        error: function(){
            alert('Impossible de sauvegarder la nouvelle note !');
        }
    });
}

function sauvegarderModifNote(){ // sauvegarde les modifications de la note dans l'API
    let note = new Note(
        Number($("#idNote").val()),
        $("#titre").val(),
        Number( $("#proprio").val() ),
        $("#contenu").val(),
        ($("#imp").attr('src') == "img/etoile_rempli.png")
    );
    console.log("PUT");
    console.log(JSON.stringify(note));
    $.ajax({
        url: urlNotes+note.id,
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(note),
        dataType: 'json',
        success: function(res){
            console.log("Résultat du PUT:", res);
            refresh();
        },
        error: function(err){
            alert('Impossible de sauvegarder la nouvelle note !');
        }
    });
}

function supprimerNote(){ // supprime la note dans l'API
    let id = Number($("#idNote").val());
    if ( id ){ // vérification qu'une note a bien été sélectionnée
        $.ajax({
            url: urlNotes+id,
            type: 'DELETE',
            contentType: 'application/json',
            dataType: 'json',
            success: function(msg){
                alert('Suppression de la note réussie !');
                refresh();
            },
            error: function(err){
                alert('Impossible de supprimer la note !');
            }
        });
    }
}

function getIdMaxNote(){ //Retourne le prochain identifiant pour une Note
    let notes = recupererNotes();
    if(notes.length > 0){
        let derniereNote = notes[notes.length-1];
        return derniereNote.id + 1;
    }
    return 1;
}

// Fonctions API proprietaire
function recupererProprios(){ //récupère tous les proprietaires de l'API
    liste = new Array();

    $.ajax({
        url: urlProprietaires,
        type: "GET",
        async: false,
        dataType : "json",
        success: function(proprios){
            proprios.forEach(function(elem) {
                liste.push(
                    new Proprietaire(elem.id, elem.nom, elem.prenom)
                );
            });
            console.log(liste);
            remplirNavProprio(liste);
            majSelectProprio(liste);
        },
        error: function(req, status, err) {
            window.alert("Impossible de récupérer les propriétaires !");
        }
    });
    return liste;
}

function recupererProprio(id){ //récupère un proprietaires de l'API
    $.ajax({
        url: urlProprietaires+id,
        type: "GET",
        async: false,
        dataType : "json",
        success: function(proprioJSON){
            let n = new Proprietaire(proprioJSON.id, proprioJSON.nom, proprioJSON.prenom)
            console.log(n);
            return n;
        },
        error: function(req, status, err) {
            window.alert("Impossible de récupérer le propriétaire "+id+" !");
        }
    });
}

function sauvegarderNouveauProprio(){ // sauvegarde le nouveau proprietaire dans l'API
    let proprio = new Proprietaire(
        getIdMaxProprio(),
        $("#nomP").val(),
        $("#prenomP").val()
    );
    console.log(JSON.stringify(proprio));
    $.ajax({
        url: urlProprietaires,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(proprio),
        dataType: 'json',
        success: function(){
            alert('Sauvegarde du nouveau proprietaire réussie !');
            refresh();
        },
        error: function(err){
            alert('Impossible de sauvegarder le nouveau proprietaire !');
        }
    });
}

function sauvegarderModifProprio(){ // sauvegarde les modifications du proprietaire dans l'API
    let proprio = new Proprietaire(
        Number($("#idP").val()),
        $("#nomP").val(),
        $("#prenomP").val()
    );
    console.log("PUT");
    console.log(JSON.stringify(proprio));
    $.ajax({
        url: urlProprietaires+proprio.id,
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(proprio),
        dataType: 'json',
        success: function(res){
            console.log("Résultat du PUT:", res);
            refresh();
        },
        error: function(err){
            alert('Impossible de sauvegarder le propriétaire !');
        }
    });
}

function supprimerProprio(){ // supprime le proprietaire dans l'API
    console.log("supprimerProprio");
    let id = Number( $("#idP").val() );
    console.log(id);
    if ( id ){ // vérification qu'une note a bien été sélectionnée
        //Suppression de toutes ses notes
        let listeNote = recupererNotes();
        for(note of listeNote){
            if (note.proprietaireId == id){
                $.ajax({
                    url: urlNotes+note.id,
                    type: 'DELETE',
                    contentType: 'application/json',
                    dataType: 'json',
                });
            }
        }
        //Suppression du propriétaire
        $.ajax({
            url: urlProprietaires+id,
            type: 'DELETE',
            contentType: 'application/json',
            dataType: 'json',
            success: function(msg){
                alert('Suppression du propriétaire réussie !');
                refresh();
            },
            error: function(err){
                alert('Impossible de supprimer le propriétaire !');
            }
        });

    }
}

function getIdMaxProprio(){ //Retourne le prochain identifiant pour un Propriétaires
    let proprios = recupererProprios();
    if(proprios.length > 0){
        let dernier = notes[proprios.length-1];
        return proprios.id + 1
    }
    return 1;
}

// Fonctions de remplissage des nav
function remplirNavNotes(listeNotes){ //Remplis la liste des Notes pour la barre de navigation
    console.log("remplirNavNotes");
    $("#notes").empty();
    for(note of listeNotes){
        $("#notes").append(
            $("<li>").append(
                $("<a>")
                .text(note.titre)
                .on("click", note, afficherNote)
            )
        );
    }
}

function remplirNavProprio(listeProprio){//Remplis la liste des Propriétaires pour la barre de navigation
    console.log("remplirNavProprio");
    $("#proprios").empty();
    for(proprio of listeProprio){
        $("#proprios").append(
            $("<li>").append(
                $("<a>")
                .text(proprio.nom+" "+proprio.prenom)
                .on("click", proprio, afficherProprio)
            )
        );
    }
}

function afficherNote(event){//Remplissage du formulaire d'après une note
    console.log("afficherNote");

    $("#sauvegarder").attr("onclick", "sauvegarderModifNote()");
    $("#titre").val(event.data.titre);
    $("#contenu").val(event.data.contenu);
    $("#idNote").val(event.data.id);

    if( event.data.important ){
        $("#imp").attr("src", "img/etoile_rempli.png");
    }
    else{
        $("#imp").attr("src", "img/etoile_vide.png");
    }

    $("#proprio").val(event.data.proprietaireId);
    $("#supprimer").css("visibility", "visible");
}

function affichageNewNote(){//Remplissage du formulaire pour une nouvelle note
    $("#sauvegarder").attr("onclick", "sauvegarderNouvelleNote()");

    $("#imp").attr("src", "img/etoile_vide.png");
    $("#idNote").val("");
    $("#titre").val("");
    $("#contenu").val("");
    $("select#proprio").val(1);
    $("#supprimer").css("visibility", "hidden");
}

function afficherProprio(event){//Remplissage du formulaire d'après un Propriétaire
    console.log("afficherProprio");

    $("#sauvegarderP").attr("onclick", "sauvegarderModifProprio()");
    $("#idP").val(event.data.id);
    $("#nomP").val(event.data.nom);
    $("#prenomP").val(event.data.prenom);

    $("#supprimerP").css("visibility", "visible");
}

function affichageNewProprio(){//Remplissage du formulaire pour un nouveau Propriétaire
    $("#sauvegarderP").attr("onclick", "sauvegarderNouveauProprio()");

    $("#idP").val("");
    $("#nomP").val("");
    $("#prenomP").val("");
    $("#supprimerP").css("visibility", "hidden");
}

//Fonctions autres
function refresh(){ //recharge les informationsde la page
    recupererNotes();
    recupererProprios();

    affichageNewNote();
    affichageNewProprio();
    $("#supprimer").css("visibility", "hidden");
    $("#supprimerP").css("visibility", "hidden");
}

function majSelectProprio(listeProprio){ //Met à jour la liste select de propriétaire pour les notes
    console.log("majSelectProprio");
    $("select#proprio").empty();
    for(proprio of listeProprio){
        console.log(proprio);
        $("select#proprio").append(
            new Option(
                proprio.nom+" "+proprio.prenom,
                proprio.id
            ));
    }
}

function changeImportant(){ //Change le status d'une note : important ou non
    console.log("changeImportant");

    if( $("#imp").attr('src') == "img/etoile_rempli.png" ){
        $("#imp").attr("src", "img/etoile_vide.png");
    }
    else{
        $("#imp").attr("src", "img/etoile_rempli.png");
    }
}
